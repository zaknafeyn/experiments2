﻿using System;
using BinarySearch.UnitTest.Misc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinarySearch.UnitTest
{
  [TestClass]
  public class BinarySearchTest
  {
    ComparableItem [] comparableitems = new[]
                              {
                                new ComparableItem(1),
                                new ComparableItem(2),
                                new ComparableItem(5),
                                new ComparableItem(12),
                              };
    [TestMethod]
    public void ShouldNotFind()
    {
      var searcher = new BinarySearcher<ComparableItem>(comparableitems);
      var result = searcher.Search(new ComparableItem(8), comparableitems);
      Assert.AreEqual(-1, result);
    }

    [TestMethod]
    public void ShouldFind()
    {
      var searcher = new BinarySearcher<ComparableItem>(comparableitems);
      var result = searcher.Search(new ComparableItem(2), comparableitems);
      Assert.IsTrue(result >= 0);
    }

    [TestMethod]
    public void ShouldFindFirst()
    {
      var searcher = new BinarySearcher<ComparableItem>(comparableitems);
      var result = searcher.Search(new ComparableItem(1), comparableitems);
      Assert.IsTrue(result == 0);
    }

    [TestMethod]
    public void ShouldFindLast()
    {
      var searcher = new BinarySearcher<ComparableItem>(comparableitems);
      var result = searcher.Search(new ComparableItem(12), comparableitems);
      Assert.IsTrue(result == comparableitems.Length-1);
    }

    [TestMethod]
    public void ShouldNotFindOutOfArr()
    {
      var searcher = new BinarySearcher<ComparableItem>(comparableitems);
      var result = searcher.Search(new ComparableItem(-10), comparableitems);
      Assert.IsTrue(result < 0);
    }
  }
}
