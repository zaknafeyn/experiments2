﻿using System;

namespace BinarySearch.UnitTest.Misc
{
  public class ComparableItem : IComparable, IComparable<ComparableItem>
  {
    private int i;
    public ComparableItem (int i)
    {
      this.i = i;
    }
    public int CompareTo(object obj)
    {
      return CompareTo((ComparableItem) obj);
    }

    public int CompareTo(ComparableItem other)
    {
      if (i < other.i) return -1;

      if (i == other.i) return 0;

      return 1;
    }
  }
}