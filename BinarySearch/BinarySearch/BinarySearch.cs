﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BinarySearch
{
  public class BinarySearcher<T> where T : IComparable
  {
    public BinarySearcher(params T[] sortedItems)
    {
      // make sure array is sorted
      if (!IsSorted(sortedItems))
        throw new InvalidDataException("Input data is not sorted, binary search cannot be performed");


    }

    private bool IsSorted(T[] sortedItems)
    {
      if (sortedItems.Length == 0)
        return true;

      var prevItem = sortedItems[0];
      foreach (var sortedItem in sortedItems)
      {
        if (prevItem.CompareTo(sortedItem) > 0)
          return false;

        prevItem = sortedItem;
      }

      return true;
    }

    public int Search(T itemToFind, T[] sortedItems)
    {
      int left = 0;
      int right = sortedItems.Length - 1;
      int middle;

      while (left <= right)
      {
        middle = left + (right - left) / 2;
        if (IsEqual(itemToFind, sortedItems[middle]))
          return middle;

        if (IsBigger(itemToFind, sortedItems[middle]))
        {
          left = middle + 1;
        }
        else
        {
          right = middle - 1;
        }
      }

      return -1;
    }

    private bool IsBigger(T item, T itemToCompare)
    {
      return item.CompareTo(itemToCompare) > 0;
    }

    private bool IsEqual(T item, T itemToCompare)
    {
      return item.CompareTo(itemToCompare) == 0;
    }

  }
}